import os
import string
from pyquery import PyQuery as pq
from lxml import etree
import sys
import struct
import codecs

scriptPath = os.path.dirname(os.path.realpath(__file__))

class GCNParser:


	def __init__(self):
		self.id = ""
		self.name = ""
		self.region = ""
		self.regions = { 
			"E":"NTSC-U", 
			"J":"NTSC-J", 
			"P":"PAL", 
			"D":"PAL", 
			"U":"PAL", 
			"X":"PAL",
			"Y":"PAL", 
			"Z":"PAL" 
			}


	def parse(self,iso_name):

		iso = open(iso_name,'r+b')

		# compressed wii isos start at a fixed offset
		if iso_name.endswith('.ciso'):
			iso.seek(0x8000)

		# get game id
		buf = iso.read(6)
		self.id = buf.upper()

		# find disc id
		discID = struct.unpack('<b',iso.read(1))[0]

		# get region
		try:
			self.region = self.regions[ buf[-3] ]
		except KeyError:
			self.region = self.regions['E']
		except:
			raise

		iso.seek( iso.tell()+0x19 )

		# get game title
		name = iso.read(50).strip().replace("\x00",'')

		""" DEPRECATED, replaced with local ID list
		# get proper game title from GameTDB
		tdb = pq( ("http://www.gametdb.com/Wii/%s" % self.id) )
		row = tdb("td:contains('title (DE)')").parent()
		titleDE = row('td:last').text()

		if titleDE:
			name = titleDE
		else:
			row = tdb("td:contains('title (EN)')").parent()
			titleEN = row('td:last').text()
			if titleEN:
				name = titleEN
		"""
		# find proper game title in local list
		with codecs.open(scriptPath+r"\ids.txt",'r',"utf-8") as f:
			for game in f:
				entry = game.split(' = ')

				if entry[0] == self.id:
					name = entry[1].strip()
					break

		# remove characters forbidden in windows filenames
		name = name.replace(':',' -').replace('/','-')
		for char in '*?"<>|':
			name = name.replace(char,'')

		#name = unicode(name, errors='ignore')
		name = ' '.join(name.split())

		# append disc id
		if discID:
			discID = discID+1
			name = name + " (Disc " + str(discID) + ")"

		self.name = name
		iso.close()

		return None


def main():

	if not len(sys.argv[1]):
		print "Please set a folder containing game ISOs"
		sys.exit(0)

	os.chdir( sys.argv[1] )

	for files in os.listdir("."):
	    if files.endswith( (".iso",".ciso",".gcm") ):

			gcn = None
			gcn = GCNParser()

			gcn.parse(files)
			try:
				if files.endswith('.ciso'):
					print "ren \"%s\" \"%s [%s] [%s].ciso\"" % (files.decode('utf-8'),gcn.name,gcn.id,gcn.region)
				else:
					print "ren \"%s\" \"%s [%s] [%s].iso\"" % (files.decode('utf-8'),gcn.name,gcn.id,gcn.region)
			except Exception, e:
				print '------'
				print files, gcn.id
				print str(e)
				print '------'


if __name__ == "__main__":
    main()