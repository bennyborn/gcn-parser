# gcn-parser #
Opens Gamecube ISO and GCM files and displays the games original title and id as well as the region code.

## Further information
With the game ID further information about the game can be obtained from GameTDB. Just add the ID at the end of the url `http://www.gametdb.com/Wii/YOURGAMEID` like [http://www.gametdb.com/Wii/GTFPA4](http://www.gametdb.com/Wii/GTFPA4 "http://www.gametdb.com/Wii/GTFPA4")

## Warning
This implementation may not work for all games. I couldn't find any documentation on the iso format used for these games so everything was investigated on my own.